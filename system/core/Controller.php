<?php
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP
 *
 * This content is released under the MIT License (MIT)
 *
 * Copyright (c) 2014 - 2016, British Columbia Institute of Technology
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @package	CodeIgniter
 * @author	EllisLab Dev Team
 * @copyright	Copyright (c) 2008 - 2014, EllisLab, Inc. (https://ellislab.com/)
 * @copyright	Copyright (c) 2014 - 2016, British Columbia Institute of Technology (http://bcit.ca/)
 * @license	http://opensource.org/licenses/MIT	MIT License
 * @link	https://codeigniter.com
 * @since	Version 1.0.0
 * @filesource
 */
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Application Controller Class
 *
 * This class object is the super class that every library in
 * CodeIgniter will be assigned to.
 *
 * @package		CodeIgniter
 * @subpackage	Libraries
 * @category	Libraries
 * @author		EllisLab Dev Team
 * @link		https://codeigniter.com/user_guide/general/controllers.html
 */


class CI_Controller {

	/**
	 * Reference to the CI singleton
	 *
	 * @var	object
	 */
	private static $instance;

	/**
	 * Class constructor
	 *
	 * @return	void
	 */
	public function __construct()
	{
		self::$instance =& $this;

		// Assign all the class objects that were instantiated by the
		// bootstrap file (CodeIgniter.php) to local class variables
		// so that CI can run as one big super object.
		foreach (is_loaded() as $var => $class)
		{
			$this->$var =& load_class($class);
		}

		$this->load =& load_class('Loader', 'core');
		$this->load->initialize();
		log_message('info', 'Controller Class Initialized');

		$this->load->helper('url');

		//self::auth_user();
	}

	// --------------------------------------------------------------------

	/**
	 * Get the CI singleton
	 *
	 * @static
	 * @return	object
	 */
	public static function &get_instance()
	{
		return self::$instance;
	}

	public static function initLanguageAndSeo(){
        $array_meta_title = [];
        $array_meta_desc= [];

        if($_COOKIE['lang']) {
            require_once FCPATH . '/application/language/' . $_COOKIE['lang'] . '.php';
            require_once FCPATH . '/application/seo/title_' . $_COOKIE['lang'] . '.php';
            require_once FCPATH . '/application/seo/meta_desc_' . $_COOKIE['lang'] . '.php';
        } else {
            $lang = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2);
            if($lang == 'en') {
                require_once FCPATH . '/application/language/en.php';
                require_once FCPATH . '/application/seo/title_en.php';
                require_once FCPATH . '/application/seo/meta_desc_en.php';
            }elseif($lang == 'ru' || $lang == 'ua') {
                require_once FCPATH . '/application/language/ru.php';
                require_once FCPATH . '/application/seo/title_ru.php';
                require_once FCPATH . '/application/seo/meta_desc_ru.php';
            } else {
                require_once FCPATH . '/application/language/en.php';
                require_once FCPATH . '/application/seo/title_en.php';
                require_once FCPATH . '/application/seo/meta_desc_en.php';
            }
        }


        @$seo_meta = ['title' => $array_meta_title[$_SERVER['REQUEST_URI']], 'meta_desc' => $array_meta_desc[$_SERVER['REQUEST_URI']]];

        return $seo_meta;
	}


	public static function get_seo(){
		$db = &get_instance()->db;
		$language = CI_Controller::language();
		$result = $db->select('title_' . $language . ' title, meta_' . $language . ' meta, h1_' . $language . ' h1, h2_' . $language . ' h2, text_' . $language . ' text')->where('url', $_SERVER['REQUEST_URI'])->get('seo_pages')->result_array();
		return $result;
	}

	public static function get_seo1(){
		$alias = explode('/', $_SERVER['REQUEST_URI']);
		$alias = array_pop($alias);

		$db = &get_instance()->db;
		$language = CI_Controller::language();
		$result = $db->select('title_' . $language . ' title, meta_' . $language . ' meta')->where('alias', $alias)->get('radiostations')->result_array();
		return $result;
	}


	public function auth_user(){
		if(isset($_GET['uid'])){
			$db = &get_instance()->db;
			$result = $db->select('first_name, last_name, picture')->where('uid', $_GET['uid'])->get('users')->result_array();

			if(empty($result)) {
				$data = array(
					'uid' => $_GET['uid'],
					'first_name' => $_GET['first_name'],
					'last_name' => $_GET['last_name'],
					'picture' => $_GET['photo_rec']
				);

				$this->db->insert('users', $data);
			} else {
				echo "<pre>";
				var_dump($_GET);
				echo "</pre>";
			}

		}
	}

	public function var_dump_pre($mixed = null) {
		echo '<pre>';
		var_dump($mixed);
		echo '</pre>';
		return null;
	}



}

<?php
/**
 * Created by PhpStorm.
 * User: Киба
 * Date: 05.08.2016
 * Time: 20:07
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('MainModel');
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->load->library('session');
        $this->load->library('pagination'); // Бибилотека пейджера
        $this->load->helper('security');
        $this->seo = CI_Controller::initLanguageAndSeo();

    }

    public function index(){
        $data['seo'] = $this->seo;


        $this->load->view('header');
        $this->load->view('index', $data);
        $this->load->view('footer');
    }


    public function error404() {

        header("HTTP/1.0 404 Not Found");

        $this->load->view('error404');
    }

    public function write_to_us(){
        $post = $this->input->post();

        if($post){
            $this->MainModel->write_to_us_func($post);
            echo json_encode(true);
            exit;
        } else {
            redirect(base_url('main/error404'));
        }
    }

}
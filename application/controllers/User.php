<?php
/**
 * Created by PhpStorm.
 * User: Киба
 * Date: 05.08.2016
 * Time: 20:07
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('UserModel');
        $this->load->helper('url');
        $this->load->helper('form');
        //$this->load->library('input');
        $this->load->library('form_validation');
        $this->load->library('session');
        //$this->load->library('pagination'); // Бибилотека пейджера
        $this->load->helper('security');

        $this->seo = CI_Controller::initLanguageAndSeo();


    }

    public function signin(){
        $data['seo'] = $this->seo;
        $post = $this->input->post();

        if($post) {
            $this->UserModel->savePreviewFunc();
            $this->UserModel->saveUser($post);
            redirect(base_url('UsersList/show/signinSuccess'));
        } else {
            $this->load->view('header');
            $this->load->view('signin');
            $this->load->view('footer');
        }

    }

    public function login($success = NULL){
        $data['seo'] = $this->seo;
        $post = $this->input->post();

        if($post) {
            $result = $this->UserModel->authorizeUser($post);

            if($result) {
                redirect(base_url('UsersList/show'));
            } else {
                redirect(base_url('user/login/failed'));
            }

        } else {
            $this->load->view('header');
            $this->load->view('login');
            $this->load->view('footer');
        }

    }

    //Загрузка картинок
   public function download_picture() {
        if ($_FILES['file']) {
            $result = $this->UserModel->saveImage();
            echo json_encode($result);
            exit;
        } else {
            redirect(base_url('main/error404'));
            //$data = json_encode($this->upload->display_errors());
        }


    }


    public function is_email_exists(){
        $post = $this->input->post('email');

        if($post) {
            $data = $this->UserModel->is_email_exists_check($post);
            if(empty($data)) $result =  true;
            else $result =  false;

            echo json_encode($result);
            exit;
        } else {
            redirect(base_url('main/error404'));
        }
    }

    public function get_similar_hobby(){
        $post = $this->input->post('hobby');

        if($post){
            $data = $this->UserModel->get_similar_hobby_func($post);
            echo json_encode($data);
            exit;
            //CI_Controller::var_dump_pre($data);
        } else {
            redirect(base_url('main/error404'));
        }
    }

    public function get_similar_country(){
        $post = $this->input->post('country');

        if($post){
            $data = $this->UserModel->get_similar_country_func($post);
            echo json_encode($data);
            exit;
            //CI_Controller::var_dump_pre($data);
        } else {
            redirect(base_url('main/error404'));
        }
    }


    public function restore_password(){
        $post = $this->input->post('email');

        if($post){
            $data = $this->UserModel->restore_password_func($post);
            echo json_encode($data);
            exit;
            //CI_Controller::var_dump_pre($data);
        } else {
            redirect(base_url('main/error404'));
        }
    }

    public function profile(){

    }

    public function logout(){
        $this->session->unset_userdata('user_id');
        redirect(base_url());
    }

}
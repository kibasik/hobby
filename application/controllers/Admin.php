<?php
defined('BASEPATH') OR exit('No direct script access allowed');



class Admin extends CI_Controller
{


    public function __construct()
    {
        parent::__construct();
        $this->load->model('AdminModel');
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->load->library('session');
        $this->load->helper('form');
        $this->language =  CI_Controller::language();

        if($this->router->fetch_method() != "index") {
            if(empty($_SESSION['admin'])) redirect(base_url() . 'admin');
        }

    }



    public function index()
    {
        if(isset($_POST['email'])) {
            $result = $this->AdminModel->checkAdmin();
        }

        $this->load->view('admin/admin_auth');
    }

    public function main()
    {

        $this->load->view('admin/header.php');
    }


    public function  setUser(){

       /* $data = array(
            'email' => 'tegs@i.ua',
            'login' => 'tegs',
            'password' => sha1('yy123yorc189' . "billy1jean2is3not4my5love6")
        );

        $result = $this->db->insert('admin_users', $data);

        if($result) print_r('ok');
       */
    }

    public function Logout(){
        unset($_SESSION['admin']);
        redirect(base_url());
    }

//========================================== SEO
    public function pagesSeo($deleted = NULL){
        $data['seo'] = $this->AdminModel->get_seo_pages();

        if(isset($deleted)) $data['deleted'] = TRUE;
        else $data['deleted'] = FALSE;

        $this->load->view('admin/header' );
        $this->load->view('admin/seo_pages', $data);
    }

    public function editSeoPage($id){

        if(isset($_POST['meta_title'])) {
            $this->AdminModel->update_page_seo($id);
            redirect(base_url() . 'admin/pagesSeo/deleted');
        }

        $data['seo'] = $this->AdminModel->get_seo_for_page($id);
        $data['language'] = CI_Controller::language();

        $this->load->view('admin/header' );
        $this->load->view('admin/seo_one_page', $data);
    }
//==========================================================


}
<div class="container" id="main_container">
    <div class="row">
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12" id="main_logo_block">

        </div>

         <div class="col-lg-9 col-md-9 col-sm-6 col-xs-12" >
             <div class="row" id="userslist_main_title">
                 <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" >
                     <h1>Участники</h1>
                 </div>
                 <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" id="userlist_how_many_found">
                    <p>Найдено: 241</p>
                 </div>
             </div>
            <div class="row">
                 <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 main_people_wrapper" >
                    <div class="main_people_subwrapper">
                        <img  class="main_people_pic" src="/application/images/users_preview/Nk9-7aVbx4E.jpg_thumb.jpg" />
                        <div class="main_people_desc">
                            <h4  class="main_people_name">Олег</h4>
                            <p>Ванкувер</p>
                            <div class="main_hobbies_block">
                                <span class="badge-custom">Кометы</span>
                                <span class="badge-custom">Котики</span>
                                <span class="badge-custom">БДСМ</span>
                                <span class="badge-custom badge-hidden" style="display:none">Котики</span>
                                <span class="badge-custom badge-hidden" style="display:none">БДСМ</span>
                                <span class="badge-custom badge-hidden" style="display:none">БДСМ</span>
                                <span class="badge-custom badge-hidden" style="display:none">БДСМ</span>
                                <span class="badge-custom badge-hidden" style="display:none">БДСМ</span>
                            </div>
                        </div>
                    </div>
                </div>
                 <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 main_people_wrapper">
                    <div class="main_people_subwrapper">
                        <img  class="main_people_pic" src="/application/images/users_preview/Nk9-7aVbx4E.jpg_thumb.jpg" />
                        <div class="main_people_desc">
                            <h4  class="main_people_name">Олег</h4>
                            <p>Ванкувер</p>
                            <div class="main_hobbies_block">
                                <span class="badge-custom">Кометы</span>
                                <span class="badge-custom">Котики</span>
                                <span class="badge-custom">БДСМ</span>
                                <span class="badge-custom" style="display:none">Котики</span>
                                <span class="badge-custom" style="display:none">БДСМ</span>
                            </div>
                        </div>
                    </div>
                </div>
                 <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 main_people_wrapper" >
                    <div class="main_people_subwrapper">
                        <img  class="main_people_pic" src="/application/images/users_preview/Nk9-7aVbx4E.jpg_thumb.jpg" />
                        <div class="main_people_desc">
                            <h4  class="main_people_name">Олег</h4>
                            <p>Ванкувер</p>
                            <div class="main_hobbies_block">
                                <span class="badge-custom">Кометы</span>
                                <span class="badge-custom">Котики</span>
                                <span class="badge-custom">БДСМ</span>
                                <span class="badge-custom" style="display:none">Котики</span>
                                <span class="badge-custom" style="display:none">БДСМ</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
             <div class="row" style="margin-top:20px; margin-bottom: 20px">
                 <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 main_people_wrapper" >
                    <div class="main_people_subwrapper">
                        <img  class="main_people_pic" src="/application/images/users_preview/Nk9-7aVbx4E.jpg_thumb.jpg" />
                        <div class="main_people_desc">
                            <h4  class="main_people_name">Олег</h4>
                            <p>Ванкувер</p>
                            <div class="main_hobbies_block">
                                <span class="badge-custom">Кометы</span>
                                <span class="badge-custom">Котики</span>
                                <span class="badge-custom">БДСМ</span>
                                <span class="badge-custom" style="display:none">Котики</span>
                                <span class="badge-custom" style="display:none">БДСМ</span>
                            </div>
                        </div>
                    </div>
                </div>
                 <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 main_people_wrapper" >
                    <div class="main_people_subwrapper">
                        <img  class="main_people_pic" src="/application/images/users_preview/Nk9-7aVbx4E.jpg_thumb.jpg" />
                        <div class="main_people_desc">
                            <h4  class="main_people_name">Олег</h4>
                            <p>Ванкувер</p>
                            <div class="main_hobbies_block">
                                <span class="badge-custom">Кометы</span>
                                <span class="badge-custom">Котики</span>
                                <span class="badge-custom">БДСМ</span>
                                <span class="badge-custom" style="display:none">Котики</span>
                                <span class="badge-custom" style="display:none">БДСМ</span>
                            </div>
                        </div>
                    </div>
                </div>
                 <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 main_people_wrapper" >
                    <div class="main_people_subwrapper">
                        <img  class="main_people_pic" src="/application/images/users_preview/Nk9-7aVbx4E.jpg_thumb.jpg" />
                        <div class="main_people_desc">
                            <h4  class="main_people_name">Олег</h4>
                            <p>Ванкувер</p>
                            <div class="main_hobbies_block">
                                <span class="badge-custom">Кометы</span>
                                <span class="badge-custom">Котики</span>
                                <span class="badge-custom">БДСМ</span>
                                <span class="badge-custom" style="display:none">Котики</span>
                                <span class="badge-custom" style="display:none">БДСМ</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>




<div id="user_list_signin_dialog">Спасибо за регистрацию. <br /> Надеемся, что у нас на сайте, вы найдете приятеля по хобби!</div>
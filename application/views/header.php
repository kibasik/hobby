﻿<!DOCTYPE html> 
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta charset="utf-8">
    <meta name="description" content="<?php if(!empty($this->seo['meta_desc'])) echo $this->seo['meta_desc'] ?>">
    <meta name="yandex-verification" content="" />
    <title><?php if(!empty($this->seo['title'])) echo $this->seo['title']; ?></title>
    <link rel="shortcut icon" href="<?php echo base_url(); ?>application/images/icons/favicon.ico" type="image/x-icon">
    <link href="<?php echo base_url(); ?>application/libraries/bootstrap-4.0.0-dist/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url(); ?>application/libraries/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>application/libraries/Jcrop/css/jquery.Jcrop.css" type="text/css" />
    <link href="<?php echo base_url(); ?>application/libraries/jquery-ui-1.11.4.custom/jquery-ui.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url(); ?>application/css/style.css" rel="stylesheet" type="text/css">

</head>
<body>
    <div id="wrapper">
        <header>
            <div class="container-fluid" id="header_main">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-2 col-md-2 col-sm-4 col-xs-6" id="main_logo_block">
                            <a href="<?php echo  base_url(); ?>">
                                <img id="img-logo" class="img-fluid" src="/application/images/logo2.png" />
                            </a>
                        </div>
                        <div class="col-lg-10 col-md-10 col-sm-8 col-xs-12" id="log_reg_block">
                            <div id="main_menu">
                                <ul class="list-inline">
                                    <li class="list-inline-item main_menu_item"><a class="header_menu_button" href="<?php echo  base_url(); ?>">ГЛАВНАЯ</a></li>
                                    <li class="list-inline-item main_menu_item"><a class="header_menu_button" href="<?php echo  base_url('UsersList/show'); ?>">УЧАСТНИКИ</a></li>
                                </ul>
                            </div>

                            <div id="reg_login_menu">
                                <ul class="list-inline">
                                    <?php
                                        if(isset($this->session->user_id)) {
                                            echo ' <li class="list-inline-item main_menu_item"><a class="header_menu_button" href="' .  base_url('user/profile') . '"><i class="fa fa-user-o login_user_header-icon"  aria-hidden="true"></i>Мой профиль</a></li>';
                                            echo '<li class="list-inline-item main_menu_item"><a class="header_menu_button" href="' .  base_url('user/logout') . '"><i class="fa fa-power-off login_user_header-icon" aria-hidden="true"></i>Выход</a></li>';
                                        } else {
                                            echo ' <li class="list-inline-item main_menu_item"><a class="header_menu_button" href="' .  base_url('user/login') . '">Вход</a></li>';
                                            echo '<li class="list-inline-item main_menu_item"><a class="header_menu_button" href="' .  base_url('user/signin') . '">Регистрация</a></li>';
                                        }
                                    ?>
                                    <li class="list-inline-item main_menu_item">
                                        <select class="custom-select form-control-sm" id="main_select_lang">
                                            <option value="en" <?php if($_COOKIE['lang'] == 'en') echo 'selected="selected"'; ?>>EN</option>
                                            <option value="ru" <?php if($_COOKIE['lang'] == 'ru') echo 'selected="selected"'; ?>>RU</option>
                                        </select>
                                    </li>
                                </ul>
                            </div>

                            <!-- <span>RU</span> -->
                        </div>
                    </div>
                </div>
            </div>
        </header>




<div class="container" id="main_container" >
    <?php echo  form_open(base_url() . "user/login" , array( 'method' => 'post', 'id' => 'login_form',)) ?>
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 alert alert-danger" id="login_error_field"></div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 alert alert-success" id="login_resotre_password_success"></div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <input type="text" name="email" class="form-control login_main_inputs"  placeholder="Ваш email">
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="login_forgot_password_block">
                    <span id="login_forgot_password">Забыли пароль? </span>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <input type="password" name="password" class="form-control login_main_inputs" placeholder="Пароль">
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="login_bottom_text">
                    <p>У вас еще нет учетной записи? <a href="<?php echo base_url('user/signin'); ?>">Зарегистрироваться</a></p>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="login_submit_button_block">
                    <button type="submit" class="btn btn-primary" id="login_submit_button">Войти</button>
                </div>
            </div>
        <?php echo form_close() ?>
</div>

<div id="login_forgot_pass_dialog">
    <div id="login_forgot_pass_dialog_block">
        <div id="login_resotre_password_result"></div>
        <input type="text" name="email" class="form-control login_resotre_pass_email"  placeholder="Ваш email" style="margin-bottom:15px;">
        На ваш имейл будет отправлен новый пароль. Продолжить?
    </div>
</div>
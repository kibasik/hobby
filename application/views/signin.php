<div class="container" id="main_container">
    <?php echo  form_open(base_url() . "user/signin" , array( 'method' => 'post', 'id' => 'signin_form')) ?>
        <div id="container-signin" class="">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div id="sigin_title">
                        <h1>Зерегистрирутесь, чтоб получить возможность полноценно пользоватся сервисом</h1>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="signin_main_fields_title">
                        <p>Обязательные поля</p>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div id="signin_error_field" class="alert alert-danger"></div>
                    <input type="text" name="username" class="form-control sign_in_main_inputs"  placeholder="Ваше имя">
                    <input type="text" name="email" class="form-control sign_in_main_inputs"  placeholder="Ваш email">
                    <input type="password" name="password" class="form-control sign_in_main_inputs" placeholder="Пароль">
                </div>
            </div>
           <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="signin_main_title_hobby">
                        <p>Напишите ваше хобби</p>
                        <p><span id="signin_max_hobby_alert">Вы можете добавить не более 5 штук. В каждом слове не менее 3 букв.</span> </p>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="signin_block_hobby_tags">

                </div>
            </div>
           <div class="row">
                <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12" >
                    <input type="text" class="form-control autocomplete_add_hobby" id="signin_hobby_tag" placeholder="Введите хобби" />
                    <input type="hidden" name="hobby[]" class="signin_hobby_tag"/>
                    <input type="hidden" name="hobby[]" class="signin_hobby_tag"/>
                    <input type="hidden" name="hobby[]" class="signin_hobby_tag"/>
                    <input type="hidden" name="hobby[]" class="signin_hobby_tag"/>
                    <input type="hidden" name="hobby[]" class="signin_hobby_tag"/>
                 </div>
                   <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 text-center" id="signin_block_hobby_button">
                       <button type="button" class="btn btn-primary" id="signin_add_hobby_tag">Добавить</button>
                   </div>
                </div>

           <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="signin_main_title_file">
                        <p>Загрузите фото</p>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="signin_upl_pic_block_main">
                    <input type="file" name="user_picture" class="form-control-file" id="sign_in_picture_upload" >
                    <div id="signin_upload_pic_bar">
                        <span>Загружаем...</span>
                        <div class="progress">
                            <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" style="width: 100%" ></div>
                        </div>
                    </div>

                    <div id="signin_block_for_upload_pic" >
                        <input type="hidden" id="x" name="x" />
                        <input type="hidden" id="y" name="y" />
                        <input type="hidden" id="w" name="w" />
                        <input type="hidden" id="h" name="h" />
                        <input type="hidden" id="picture_name" name="image" />
                    </div>
                    <!--<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" id="preview" style="width:230px;height:230px;overflow:hidden;">
                        <img src="" style="display:none"/>
                    </div>-->
                </div>
            </div>
            <!-- <input type="submit" /> -->
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="signin_add_fields_title">
                        <p>Дополнительные поля</p>
                        <p>Вам не обязательно заполнять эти данные, но это позволит сделать ваш профиль интересней другим пользователям</p>
                </div>
            </div>



            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="signin_add_data_accordion">
                    <div id="accordion_signin" class="accordion">
                        <div class="card m-b-0" style="border:none">
                            <div class="card-header collapsed signin_accordion_menu" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" style="background-color: #E3E3E3;">
                                <a class="card-title">
                                    Контактные данные
                                </a>
                                <span class="glyphicon glyphicon-ok"></span>
                            </div>
                            <div id="collapseOne" class="card-block collapse">
                                <div id="signin_addtional_data_block" >
                                    <div class="input-group signin_add_inputs_group">
                                        <input class="form-control autocomplete_add_country" id="signin_add_country" name="country"  placeholder="Страна"/>
                                    </div>
                                    <div class="input-group signin_add_inputs_group">
                                        <input type="text" class="form-control"  name="city" placeholder="Город">
                                     </div>
                                    <div class="input-group signin_add_inputs_group">
                                        <i class="fa fa-phone signin_icons"></i>
                                        <input type="tel" name="mobile_number" class="form-control" placeholder="Номер телефона" >
                                    </div>
                                    <div class="input-group signin_add_inputs_group">
                                        <i class="fa fa-skype signin_icons" style="color:#12A5F4"></i>
                                        <input type="text" name="skype" class="form-control" placeholder="Skype" >
                                    </div>

                                    <div class="row">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="signin_additional_block">
                                            <p>Социальные профили</p>
                                        </div>
                                    </div>
                                    <div class="input-group signin_add_inputs_group">
                                        <i class="fa fa-facebook-official signin_icons" style="color:#3b5998"></i>
                                        <input type="text" name="fb" class="form-control" placeholder="Ссылка на профиль Facebook" >
                                    </div>
                                    <div class="input-group signin_add_inputs_group">
                                        <i class="fa fa-vk signin_icons" style="color:#507299"></i>
                                        <input type="text" name="vk" class="form-control" placeholder="Ссылка на профиль Вконтакте" >
                                    </div>
                                    <div class="input-group signin_add_inputs_group">
                                        <i class="fa fa-instagram signin_icons" style="color:#517fa4"></i>
                                        <input type="text" name="instagram" class="form-control" placeholder="Ссылка на профиль Instagram" >
                                    </div>
                                    <div class="input-group signin_add_inputs_group">
                                        <i class="fa fa-twitter-square signin_icons" style="color:#0084b4"></i>
                                        <input type="text" name="twit" class="form-control" placeholder="Ссылка на профиль Twitter" >
                                    </div>
                                    <div class="input-group signin_add_inputs_group">
                                        <i class="fa fa-linkedin-square signin_icons" style="color:#0077B5"></i>
                                        <input type="text" name="linkedIn" class="form-control" placeholder="Ссылка на профиль Linkedin" >
                                    </div>
                                    <div class="input-group signin_add_inputs_group">
                                        <i class="fa fa-google-plus-square signin_icons" style="color:#dd4b39"></i>
                                        <input type="text" name="google" class="form-control" placeholder="Ссылка на профиль Google+" >
                                    </div>
                                    <div class="input-group signin_add_inputs_group">
                                        <i class="fa fa-users signin_icons"></i>
                                        <input type="text" name="other_social" class="form-control" placeholder="Ссылка на профиль другой социальной сети" >
                                    </div>
                                </div>
                            </div>
                            <div class="card-header collapsed signin_accordion_menu" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" style="background-color: #E3E3E3;">
                                <a class="card-title">
                                  Расскажите о себе
                                </a>
                            </div>
                            <div id="collapseTwo" class="card-block collapse">
                                <div id="signin_addtional_data_block" >
                                    <div class="row">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="signin_additional_block">
                                            <p>Выберите Ваш пол</p>
                                        </div>
                                    </div>
                                    <div class="input-group signin_add_inputs_group">
                                      <select class="form-control" name="gender" >
                                          <option>Не указан</option>
                                          <option value="M">Мужской</option>
                                          <option value="F">Женский</option>
                                      </select>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="signin_additional_block">
                                            <p>Укажите дату рождения</p>
                                        </div>
                                    </div>
                                    <div class="input-group signin_add_inputs_group">
                                        <input type="date" name="birth_date" class="form-control" value="<?php //echo date("Y-m-d");?>">
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="signin_additional_block">
                                            <p>Расскажите о себе (максимум 2000 символов)</p>
                                        </div>
                                    </div>
                                    <div class="input-group signin_add_inputs_group">
                                        <textarea name="about_me" class="form-control"  rows="3" maxlength="2000"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="signin_google_captcha">

                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="signin_do_you_have_account">
                    <p>У вас уже есть аккаунт? <a href="<?php echo base_url('user/login'); ?>">Войти</a></p>
                    <p><button type="submit" class="btn btn-lg btn-primary">Зарегистрироваться</button></p>
                </div>
            </div>
        </div>
    <?php echo form_close() ?>
</div>


<!DOCTYPE html>
<html>
<head>
    <meta name="robots" content="noindex,nofollow">
    <style>
        #fof{display:block; width:100%; padding:100px 0; line-height:1.6em; text-align:center;}
        #fof p{margin:0; padding:0; font-size:16px;}
        #fof .positioned{margin-bottom:25px;}
        #fof .positioned .hgroup{margin-bottom:50px;}
        #fof .positioned .hgroup h1, #fof .positioned .hgroup h2{display:block; width:50%; margin:0; padding:0; text-transform:uppercase;}
        #fof .positioned .hgroup h1{float:left; margin-top:-70px; font-size:220px;}
        #fof .positioned .hgroup h2{float:right; font-size:50px; text-transform:none;}
        #fof .positioned p{font-weight:bold; text-transform:uppercase;}
        #fof p.clear{padding:10px; border:1px solid #CCCCCC;}
        #fof p.clear a.go-back{float:left;}
        #fof p.clear a.go-home{float:right;}

        .hgroup h1 {
            font-size:46px;
            color:#4a5c6b;
        }

        .hgroup h2 {
            font-size:36px;
            color:#4a5c6b;
        }

        #fof p {
            font-size:26px;
            color:#4a5c6b;
            line-height:38px;
        }


    </style>
</head>

<body>

<div class="wrapper row2">
    <div id="container" class="clear">
        <section id="fof" class="clear">
            <div class="hgroup">
                <h1>Что то пошло не так!</h1>
                <h2>404 ошибка</h2>
            </div>
            <p>Не удается найти запрашиваемую вами страницу</p>
            <p><a href="javascript:history.go(-1)">Вернуться</a> или перейти на <a href="<?php echo base_url() ?>">Главную</a></p>
            <!-- ####################################################################################################### -->
        </section>
    </div>
</div>
</body>
</html>


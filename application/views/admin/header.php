<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8"/>
    <meta name="robots" content="noindex,nofollow">
    <title>Dashboard I Admin Panel</title>
    <link href="<?php echo base_url(); ?>application/libraries/bootstrap-3.3.5-dist/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>application/css/admin/layout.css" type="text/css" media="screen" />
    <!--[if lt IE 9]>
    <link rel="stylesheet" href="<?php echo base_url(); ?>application/css/admin/ie.css" type="text/css" media="screen" />
    <![endif]-->
    <script src="<?php echo base_url(); ?>application/js/jquery-3.0.0.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>application/libraries/ckeditor/ckeditor.js"></script>
    <!--<script src="//cdn.ckeditor.com/4.5.10/full/ckeditor.js"></script>-->
    <script type="text/javascript">
        $(document).ready(function(){
                /*CKEDITOR.replace( 'editor', {
                   extraPlugins: 'filebrowser',
                        filebrowserBrowseUrl: '/browser/browse.php',
                        filebrowserUploadUrl: '<?php echo base_url(); ?>/application/libraries/ckeditor/upload.php?url=<?php echo base_url(); ?>',
                    language: 'en',
                });*/

            });
    </script>


</head>

<!-- icn_new_article icn_settings icn_edit_article icn_add_user -->
<body>

<header id="header">
    <hgroup>
        <h1 class="site_title"><a href="index.php">Административная панель</a></h1>
        <h2 class="section_title"></h2><div class="btn_view_site"><a href="<?php echo base_url() ?>" target="_blank">На сайт</a></div>
    </hgroup>
</header> <!-- end of header bar -->

<section id="secondary_bar">
    <div class="user">
        <p><?php echo $_SESSION['admin']  ?></p>
        <!-- <a class="logout_user" href="#" title="Logout">Logout</a> -->
    </div>
    <div class="breadcrumbs_container">
        <article class="breadcrumbs"><a href="<?php echo base_url() ?>" target="_blank">Website</a> <div class="breadcrumb_divider"></div> <a class="current">Website Admin</a></article>
    </div>
</section><!-- end of secondary bar -->

<aside id="sidebar" class="column">
    <form class="quick_search">
        <input type="text" value="Quick Search" onfocus="if(!this._haschanged){this.value=''};this._haschanged=true;">
    </form>
    <hr/>
    <h3>Служебное</h3>
    <ul class="toggle">
        <li class="icn_settings"><a href="">Сео для страниц</a></li>
    </ul>
    <h3>Пользователи</h3>
    <ul class="toggle">
        <li class="icn_view_users"><a href="<?php echo base_url() ?>admin/addUser">Список пользователей</a></li>
        <li class="icn_folder"><a href="<?php echo base_url() ?>admin/addUser">Сообщения пользователей</a></li>
    </ul>
    <h3>Админка</h3>
    <ul class="toggle">
        <li class="icn_settings"><a href="#">Опции</a></li>
        <li class="icn_folder"><a href="#">Сообщения от пользователей</a></li>
        <li class="icn_jump_back"><a href="<?php echo base_url() ?>admin/Logout">Выйти</a></li>
    </ul>

    <footer>
        <hr />
        <p><strong></strong></p>
    </footer>
</aside><!-- end of sidebar -->


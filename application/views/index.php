<div class="container-fluid" id="main-picture-block">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="main_picture_block">
            <!-- <img  src="/application/images/6522551.jpg" /> -->
            <div id="main_picture_subblock">
                <h1><?php echo MAIN_TEXT_1 ?></h1>
                <h2>Сервис поможет Вам быстро находить интересных людей, с такими же хобби, как и у Вас</h2>
            </div>
        </div>
    </div>
</div>
<div class="container" id="main_container">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div id="how_it_works">
                <h2 id="how_it_works_children_1">Как это работает?</h2>
                <p id="how_it_works_children_2">Все очень просто...</p>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 how_it_works_block">
            <img  class="img-fluid" src="/application/images/stage1.png" />
            <p>1.Регистрируемся</p>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 how_it_works_block">
            <img  class="img-fluid" src="/application/images/stage2.png" />
            <p>2.Находим человека с аналогичным интересом</p>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 how_it_works_block">
            <img  class="img-fluid" src="/application/images/stage3.png" />
            <p>3.Налаживаем контакт</p>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 how_it_works_block" >
            <img  class="img-fluid" src="/application/images/stage4.png" />
            <p>4.Наслаждаемся совместным времяпровождением</p>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div id="main_people_title">
                <h2 id="main_people_title_children_1">Учасники</h2>
            </div>
        </div>
    </div>

    <!--- ПРИ ПОДКЛЮЧЕНИИ ПХП ОБРЕЗАТЬ КОЛИЧЕСТВО СИМВОЛОВ В ХОББИ ДО 8 СИМВОЛОВ ПОСЛЕ ЧЕГО 3 ТОЧКИ. ИНАЧЕ БУДЕТ ВЫЛАЗИТЬ ИЗ КАРТОЧКИ !!!! -->
    <div class="row">
        <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 main_people_thumb">
            <div class="main_people_subwrapper">
                <img  class="main_people_pic" src="/application/images/users_preview/pepe_prototype_by_mikedoscher-d9jwkl21.jpg_thumb.jpg" />
                <div class="main_people_desc">
                    <h4  class="main_people_name">Олег</h4>
                    <p>Ванкувер</p>
                    <div class="main_hobbies_block">
                        <span class="badge-custom">Кометы12...</span>
                        <span class="badge-custom">Кометы12...</span>
                        <span class="badge-custom">Кометы12...</span>
                        <span class="badge-custom" style="display:none">Кометы12...</span>
                        <span class="badge-custom" style="display:none">Кометы12...</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 main_people_thumb">
            <div class="main_people_subwrapper">
                <img  class="main_people_pic" src="/application/images/users_preview/1-set-DON-T-TOUCH-MY-BIKE-words-warning-stickers-for-road-mountain-bike-bicycle-decals61.jpg_thumb.jpg" />
                <div class="main_people_desc">
                    <h4  class="main_people_name">Олег</h4>
                    <p>Ванкувер</p>
                    <div class="main_hobbies_block">
                        <span class="badge-custom">Кометы</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 main_people_thumb">
            <div class="main_people_subwrapper">
                <img  class="main_people_pic" src="/application/images/users_preview/AsvtY-brO7g.jpg_thumb.jpg" />
                <div class="main_people_desc">
                    <h4  class="main_people_name">Олег</h4>
                    <p>Ванкувер</p>
                    <div class="main_hobbies_block">
                        <span class="badge-custom">Кометы</span>
                        <span class="badge-custom">Котики</span>
                        <span class="badge-custom">БДСМ</span>
                        <span class="badge-custom" style="display:none">Котики</span>
                        <span class="badge-custom" style="display:none">БДСМ</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 main_people_thumb">
            <div class="main_people_subwrapper">
                <img  class="main_people_pic" src="/application/images/users_preview/Nk9-7aVbx4E.jpg_thumb.jpg" />
                <div class="main_people_desc">
                    <h4  class="main_people_name">Олег</h4>
                    <p>Ванкувер</p>
                    <div class="main_hobbies_block">
                        <span class="badge-custom">Кометы</span>
                        <span class="badge-custom">Котики</span>
                        <span class="badge-custom">БДСМ</span>
                        <span class="badge-custom" style="display:none">Котики</span>
                        <span class="badge-custom" style="display:none">БДСМ</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div id="main_show_all_people">
                <a href="">Посмотреть всех учасников</a>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div id="main_about_us_title">
                <h2 id="main_about_us_title_children_1">О сервисе</h2>
            </div>
        </div>
    </div>

    <!-- 50-70 ключевых символо на 1000 обших !!!! -->
    <div class="row ">
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <img class="img-fluid" src="/application/images/1334216373_1033.jpg" />
        </div>
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 main_about_us_block_text" >
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation
        ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint
       occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.
        </div>
    </div>

    <div class="row main_about_us_block">
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 main_about_us_block_text">
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation
  ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint
       occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.
        </div>
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" >
            <img class="img-fluid" src="/application/images/uMJ9iUvwfco.jpg" />
        </div>
    </div>


    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div id="main_find_new_people">
                <h2 id="main_find_new_people_children_1">Хочешь найти единомышленника или партнера по хобби?</h2>
                <p id="main_find_new_people_children_2">Регистрируйся, находи нужных тебе людей и проводите время вместе!</p>
                <p><button type="button" class="btn btn-lg btn-primary">Регистрация</button></p>
            </div>
        </div>
    </div>
</div>

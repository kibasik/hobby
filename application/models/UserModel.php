<?php
class UserModel extends CI_Model {

    public $language;

    public function __construct()
    {
        $this->load->database();
    }


    public function is_email_exists_check($post) {
        $data = $this->db->select("email")->from('users')->where('email', $post)->get()->result_array();
        return $data;
    }

    public function saveUser($post){
        //Подготовим данные и сохраним юзера
        $password = sha1($post['password']);
        $hobbies = json_encode($post['hobby'], JSON_UNESCAPED_UNICODE);

        $birth_date_arr = explode('-', $post['birth_date']);

        foreach($post as $key => $val) {
            if(empty($val)) {
                $post[$key] = NULL;
            }
        }

        if($post['birth_date']) {
            $user_age = $this->UserAge($birth_date_arr[0], $birth_date_arr[1], $birth_date_arr[2]);
        } else {
            $user_age = NULL;
        }


        $data = array(
            'email' => $post['email'] ,
            'password' => $password ,
            'image' => $post['image'],
            'username' => $post['username'] ,
            'birth_date' => $post['birth_date'],
            'age' => $user_age,
            'country' => $post['country'],
            'city' => $post['city'],
            'skype' => $post['skype'],
            'mobile_number' => $post['mobile_number'],
            'vk' => $post['vk'],
            'fb' => $post['fb'],
            'twit' => $post['twit'],
            'instagram' => $post['instagram'],
            'google' => $post['google'] ,
            'linkedIn' => $post['linkedIn'] ,
            'other_social' => $post['other_social'],
            'gender' => $post['gender'] ,
            'about_me' => $post['about_me'] ,
            'hobbies' => $hobbies
        );

        $this->db->insert('users', $data);

        $user_session = $this->db->insert_id();
        $this->session->set_userdata(['user_id' =>  $user_session]);


        //Здесь запишем не существующие еще хобби
        $exist_hobby_arr =[];
        foreach($post['hobby'] as $val){
            if(!empty($val)) $exist_hobby_arr[] = $val;
        }

        $data = $this->db->select("title")->from('hobbies_tags')->get()->result('array');

        $data_new = [];
        $count = count($data) - 1;
        for($i=0; $i<=$count; $i++){
            $data_new[$i] = $data[$i]['title'];
        }

        $diff = array_diff($exist_hobby_arr, $data_new);


        if(!empty($diff)){
            $data_new = [];
            $count = max(array_keys($diff));

            for($i=0; $i<=$count; $i++){
                if(isset($diff[$i])) {
                    $data_new[$i]['title'] = $diff[$i];
                }
            }

            $this->db->insert_batch('hobbies_tags', $data_new);
        }
    }

    public function authorizeUser($post){
        $password = sha1($post['password']);
        $data = $this->db->select("id")->from('users')->where('email', $post['email'])->where('password', $password)->get()->result_array();

        if($data) {
            $user_session =['user_id' =>  $data[0]['id']];
            $this->session->set_userdata($user_session);
            $result = TRUE;
        } else {
            $result = FALSE;
        }

        return $result;
    }

    public function saveImage(){
        $config['upload_path'] = FCPATH . '/application/images/users';
        $config['allowed_types'] = 'gif|jpg|png';
        $config['remove_spaces'] = TRUE;
        $config['encrypt_name'] = TRUE;
        $this->load->library('upload', $config);

        if ($this->upload->do_upload('file'))
        {

            $config['source_image'] = $this->upload->upload_path.$this->upload->file_name;
            $config['width']  = 800;
            $config['height']    = 800;
            $config['maintain_ratio'] = TRUE;
            $this->load->library('image_lib', $config);
            $this->image_lib->resize();

            $resized_image = getimagesize($this->upload->upload_path.$this->upload->file_name);
            //$data = $this->upload->data();

            $data['file_name'] = $this->upload->file_name;
            $data['new_width'] = $resized_image[0];
            $data['new_height'] = $resized_image[1];

            return $data;
        } else {
            return FALSE;
        }
    }

    //Загрузка превью
    public function savePreviewFunc(){
        $targ_w = $targ_h = 230;
        $jpeg_quality = 90;

        $src = FCPATH . '/application/images/users/' . $_POST['image'];
        $img_r = imagecreatefromjpeg($src);
        $dst_r = ImageCreateTrueColor( $targ_w, $targ_h );

        imagecopyresampled($dst_r,$img_r,0,0,$_POST['x'],$_POST['y'],
            $targ_w,$targ_h,$_POST['w'],$_POST['h']);

        //header('Content-type: image/jpeg');
        imagejpeg($dst_r,FCPATH . '/application/images/users_preview/' . $_POST['image'],$jpeg_quality);
    }

    private function UserAge($y, $m, $d) { // в качестве параметров будут год, месяц и день
        if($m > date('m') || $m == date('m') && $d > date('d'))
            return (date('Y') - $y - 1); // если ДР в этом году не было, то ещё -1
        else
            return (date('Y') - $y); // если ДР в этом году был, то отнимаем от этого года год рождения
    }


    public function get_similar_hobby_func($hobby){
        $data = $this->db->select("title")->from('hobbies_tags')->like('title', $hobby, 'both')->limit(10)->get()->result_array();
        $data_new = [];
        $count = count($data) - 1;
        for($i=0; $i<=$count; $i++){
            $data_new[$i] = $data[$i]['title'];
        }
        return $data_new;
    }

    public function get_similar_country_func($country){
        $data = $this->db->select("title_ru, title_ua, title_en")->from('countries')->like('title_ru', $country, 'both')->or_like('title_en', $country, 'both')
            ->limit(5)->get()->result_array();
        $data_new_ru = [];
        $data_new_en = [];
        $count = count($data) - 1;

        for($i=0; $i<=$count; $i++){
            $data_new_ru[$i] = $data[$i]['title_ru'];
        }

        for($i=0; $i<=$count; $i++){
            $data_new_en[$i] = $data[$i]['title_en'];
        }

        $data_new = array_merge($data_new_ru, $data_new_en);

        return $data_new;
    }

    public function restore_password_func($post){
        $email_exists = $this->is_email_exists_check($post);

        if(!empty($email_exists)) {
            $new_password = $this->generatePassword();
            $new_password_hash = sha1($new_password);

            $this->db->set('password', $new_password_hash)->where('email', $post)->update('users');

            $this->load->library('email');
            $this->email->from('admin@hobbyfellow.com', 'Oleg Bereza');
            $this->email->to($post);
            $this->email->subject('Восстановление пароля на www.hobbyfellow.com');
            $this->email->message('Здравствуйте. Ваш новый пароль: ' . $new_password . '. Спасибо что пользуетесь нашим сервисом!');
            $this->email->send();

            return true;
        } else {
            return false;
        }
    }

    function generatePassword($length = 10){
      $chars = 'abdefhiknrstyzABDEFGHKNQRSTYZ123456789';
      $numChars = strlen($chars);
      $string = '';
      for ($i = 0; $i < $length; $i++) {
        $string .= substr($chars, rand(1, $numChars) - 1, 1);
      }
      return $string;
    }

}
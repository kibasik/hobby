<?php
class AdminModel extends CI_Model {

    public $language;

    public function __construct()
    {
        $this->load->database();

    }





    public function checkAdmin()
    {
        $login = $this->db->escape_str($_POST['email']);
        $password = $this->db->escape_str($_POST['password']);

        $password = sha1($_POST['password'] . "billy1jean2is3not4my5love6");

        $result = $this->db->query("SELECT * FROM admin_users WHERE login = ? AND password = ?", array($login, $password))->result_array();

        if ($result != FALSE) {
            $_SESSION['admin'] = $result[0]['login'];


            if (isset($_POST['remember_me'])) {
                //setcookie("remember_me", 1, time() + 3600000000);
               // setcookie("email", $_POST['email'], time() + 3600000000);
                //setcookie("password", $_POST['password'], time() + 36000000);
            } else {
                //setcookie("remember_me", "", time() - 3600000000);
                //setcookie("email", "", time() - 3600000000);
                //setcookie("password", "", time() - 36000000);
            }


            redirect(base_url() . 'admin/main/');
        }
    }



    public function save_slider_pic(){
        $picture = time() . $_FILES["picture"]["name"];
        move_uploaded_file($_FILES["picture"]["tmp_name"],  FCPATH . "application/images/carousel_tmp/" . $picture);
        self::resizeImage(220, FCPATH . "application/images/carousel/" . $picture, FCPATH . "application/images/carousel_tmp/" . $picture);
        unlink(FCPATH . "application/images/carousel_tmp/" . $picture);

        $this->db->query("INSERT INTO slider_pictures(name) VALUES(?)", $picture);
    }

    public function get_seo_pages(){
        $records =   $this->db->query("SELECT * FROM seo_pages")->result_array();
        return $records;
    }

    public function get_seo_for_page($id){

        $records =   $this->db->query("SELECT * FROM seo_pages WHERE id = ?", $id)->result_array();
        return $records;
    }

    public function update_page_seo($id){
        $this->db->query("UPDATE seo_pages SET title_" . $this->language . " = ?, meta_" . $this->language . " = ?, h1_" . $this->language . " = ?, h2_" . $this->language . " = ?,
                            text_" . $this->language . " = ? WHERE id = ?", array($_POST['meta_title'], $_POST['meta_description'],$_POST['h1_title'],$_POST['h2_title'],$_POST['text'], $id));
    }


    private function resizeImage($newWidth, $targetFile, $originalFile) {

        $info = getimagesize($originalFile);
        $mime = $info['mime'];

        switch ($mime) {
            case 'image/jpeg':
                $image_create_func = 'imagecreatefromjpeg';
                $image_save_func = 'imagejpeg';
               // $new_image_ext = 'jpg';
                break;

            case 'image/png':
                $image_create_func = 'imagecreatefrompng';
                $image_save_func = 'imagepng';
               // $new_image_ext = 'png';
                break;

            case 'image/gif':
                $image_create_func = 'imagecreatefromgif';
                $image_save_func = 'imagegif';
                //$new_image_ext = 'gif';
                break;

            default:
                throw new Exception('Unknown image type.');
        }

        $img = $image_create_func($originalFile);
        list($width, $height) = getimagesize($originalFile);

        $newHeight = ($height / $width) * $newWidth;
        $tmp = imagecreatetruecolor($newWidth, $newHeight);
        imageAlphaBlending($tmp, false);
        imageSaveAlpha($tmp, true);
        imagecopyresampled($tmp, $img, 0, 0, 0, 0, $newWidth, $newHeight, $width, $height);

        //if (file_exists($targetFile)) {
           // unlink($targetFile);
       // }
        //$image_save_func($tmp, "$targetFile.$new_image_ext");
        $image_save_func($tmp, "$targetFile");
    }


}
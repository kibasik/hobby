<?php
class MainModel extends CI_Model {

    public $language;

    public function __construct()
    {
        $this->load->database();
    }

    public function write_to_us_func($post){
        $data = array(
            'name' => $post['name'] ,
            'email' => $post['email'] ,
            'text' => $post['text']
        );

        $this->db->insert('message_admin', $data);
    }


    public function search_by_field(){

        $search = $this->db->escape_str($this->input->get('data'));

        $search = preg_replace("/[^\w\x7F-\xFF\s]/", " ", $search);

        $search = strip_tags(trim(substr($search, 0, 64)));


        //$records['news_result'] = $this->db->query("SELECT name_" . $this->language . " AS name_news,  short_description_" . $this->language . " AS short_desc, alias, picture, news_date FROM music_news
                                                  //  WHERE (MATCH (full_description_" . $this->language . ") AGAINST ('" . $search . "') OR name_" . $this->language . " LIKE '%" . $search . "%') AND active = 1 ")->result_array();


        $records['radio_result'] = $this->db->query("SELECT r.name_" . $this->language . " AS name_radio, r.alias, r.picture, r.website, r.country_" .   $this->language . " AS country, g.name_" .   $this->language . " AS genre_name FROM radiostations r
                                                    LEFT JOIN genre g ON g.id = r.genre
                                                    WHERE (MATCH (r.description_" . $this->language . ") AGAINST ('" . $search . "') OR r.name_" .   $this->language . " LIKE '%" . $search . "%' OR
                                                    r.country_" .   $this->language . " LIKE '%" . $search . "%' OR g.name_" .   $this->language . " LIKE '%" . $search . "%') AND r.active = 1")->result_array();

       // var_dump($records['radio_result']);
        //die();

        //$records = array_merge( $records['news_result'], $records['radio_result']);
        //shuffle($records);

        return  $records['radio_result'];
    }



}
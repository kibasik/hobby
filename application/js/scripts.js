


/*
//Аутентефикация комментов в новостях
function auth_comment(){
	var text = document.getElementsByClassName('comment_text')[0].value;
	var human  = document.getElementById('human').value;

	if(text.length < 2 || !document.getElementById('human').classList.contains('acitve')) {
		document.getElementById('leave_your_message').style.display = "block";
	} else {
		var alias = document.getElementsByClassName('comment_text')[0].getAttribute('data-article');

		var ajax = new XMLHttpRequest();
		ajax.open('POST', '/news/save_comment/' + alias, true);
		ajax.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
		ajax.send("news_text=" + encodeURIComponent(text) + "&human=" + encodeURIComponent(human));
		ajax.onreadystatechange = function() { // Ждём ответа от сервера
			if (ajax.readyState == 4) { // Ответ пришёл
				if(ajax.status == 200) { // Сервер вернул код 200 (что хорошо)
					document.getElementsByClassName('reload_news_page')[0].click();
					//console.log(ajax.responseText);
				}
			}
		};
		return false;
	}
} */





	function array_unique( array ) {	// Removes duplicate values from an array
		//
		// +   original by: Carlos R. L. Rodrigues

		var p, i, j;
		for(i = array.length; i;){
			for(p = --i; p > 0;){
				if(array[i] === array[--p]){
					for(j = p; --p && array[i] === array[p];);
					i -= array.splice(p + 1, j - p).length;
				}
			}
		}

		return true;
	}



//http://deepliquid.com
  function updateCoords(c)
  {
    $('#x').val(c.x);
    $('#y').val(c.y);
    $('#w').val(c.w);
    $('#h').val(c.h);
  };


/*
	function showPreview(coords)
	{
		var rx = 100 / coords.w;
		var ry = 100 / coords.h;

		$('#preview img').css({
			width: Math.round(rx * 500) + 'px',
			height: Math.round(ry * 370) + 'px',
			marginLeft: '-' + Math.round(rx * coords.x) + 'px',
			marginTop: '-' + Math.round(ry * coords.y) + 'px'
		});
	} */

function show_login_error(){
	var full_url = window.location.href;

	var explode_url = full_url.split('/');

	if(explode_url[3] == 'user' && explode_url[4] == 'login') {
		if(explode_url[5] == 'failed') {
			$('#login_error_field').text('Неверный email или пароль');
			$('#login_error_field').show();
		}
	}
}

function show_signin_success(){
	var full_url = window.location.href;

	var explode_url = full_url.split('/');

	if(explode_url[3] == 'UsersList' && explode_url[4] == 'show') {
		if(explode_url[5] == 'signinSuccess') {
			$('#user_list_signin_dialog').dialog( "open" );
		}
	}
}


$(document).ready(function(){
	csrf_hobby_value = $("input[name=csrf_hobby_name]").val();

	show_login_error();

	$('.main_people_wrapper').hover(function(){
		$(this).find('.badge-hidden').css('display', 'inline-block');
		$(this).css('z-index', '10');
	}, function(){
		$(this).find('.badge-hidden').css('display', 'none');
		$(this).css('z-index', '0');
	});

	$( "#user_list_signin_dialog" ).dialog({
		title: "Успешная регистрация",
		modal: true,
		resizable: false,
		draggable: false,
		autoOpen: false,
		width: 'auto',
		buttons: {
			"Закрыть": function() {
				$(this).dialog("close");
			}
		}
	});

	show_signin_success();


	$('#footer_write_us_link').click(function(){
		$('#main_write_to_us_dialog').dialog( "open" );
	});

	$( "#main_write_to_us_dialog" ).dialog({
		title: "Напишите нам",
		modal: true,
		resizable: false,
		draggable: false,
		autoOpen: false,
		width: 'auto',
		open: function( event, ui ) {
			$('#main_write_to_us_result').removeClass('alert-danger');
			$('#main_write_to_us_result').removeClass('alert-success');
			$('#main_write_to_us_result').text('');
			$('#main_write_to_us_result').hide();
			$('.main_write_to_us_input').removeClass('is-invalid');
			$('.main_write_to_us_name').val('');
			$('.main_write_to_us_email').val('');
			$('.main_write_to_us_text').val('');
		},
		buttons: {
			"Подтвердить": function() {
				//Валидация заполнения имя, имейла, текста
				var error = false;
				$('.main_write_to_us_input').each(function(){
					if($(this).val().length == 0) {
						$(this).addClass('is-invalid');
						error = true;
					}
				});

				if(error) {
					$('#main_write_to_us_result').addClass('alert-danger');
					$('#main_write_to_us_result').text('Заполните обязательные поля');
					$('#main_write_to_us_result').show();
					return false;
				}


				var name = $('.main_write_to_us_name').val();
				var email = $('.main_write_to_us_email').val();
				var text = $('.main_write_to_us_text').val();

				$.ajax({
					url: '/main/write_to_us',
					dataType: 'json',
					data: {name:name, email: email, text:text, csrf_hobby_name:csrf_hobby_value},
					type: 'post',
					success: function(data) {
						if (data === true) {
							$('#main_write_to_us_result').addClass('alert-success');
							$('#main_write_to_us_result').text('Спасибо за ваше сообщение.');
							$('#main_write_to_us_result').show();

							//setTimeout(function(){
								//$("#main_write_to_us_dialog").dialog("close");
							//}, 5000);
						}
					}
				});
			},
			"Отмена": function() {
				$(this).dialog("close");
			}
		}
	});

	$('#main_select_lang').change(function(){
		var lang = $(this).val();
		var host = document.location.origin;

		var date = new Date(new Date().getTime() + 6000 * 10000000);
		document.cookie = "lang=" + lang + "; path=/; expires=" + date.toUTCString();
		location.reload();
	});


	$( "#login_forgot_pass_dialog" ).dialog({
		title: "Восстановление пароля",
		modal: true,
		resizable: false,
		draggable: false,
		autoOpen: false,
		width: 'auto',
		open: function( event, ui ) {
			$('#login_resotre_password_result').text('');
			$('#login_resotre_password_result').hide();
			$('#login_resotre_password_result').removeAttr('class');
			$('.login_resotre_pass_email').val('');
		},
		buttons: {
			"Подтвердить": function() {
				var email = $('.login_resotre_pass_email').val();

				if(email.length === 0) {
					$('.login_resotre_pass_email').addClass('is-invalid');
					$('#login_resotre_password_result').text('Напишите ваш Email');
					$('#login_resotre_password_result').show();
					$('#login_resotre_password_result').removeClass('alert-success')
					$('#login_resotre_password_result').addClass('alert alert-danger');
					return false;
				}

				$.ajax({
					url: '/user/restore_password',
					dataType: 'json',
					data: {email: email, csrf_hobby_name:csrf_hobby_value},
					type: 'post',
					success: function(data) {
						if (data === true) {
							$('#login_resotre_password_result').removeClass('alert-danger');
							$('#login_resotre_password_result').addClass('alert alert-success');
							$('#login_resotre_password_result').text('Новый пароль выслан на ваш email.');
							$('#login_resotre_password_result').show();


							//setTimeout(function(){
								//$("#login_forgot_pass_dialog").dialog("close");
							//}, 5000);

						} else {
							$('#login_resotre_password_result').removeClass('alert-success');
							$('#login_resotre_password_result').text('Email не существует');
							$('#login_resotre_password_result').show();
							$('#login_resotre_password_result').addClass('alert alert-danger');
						}
					}
				});
			},
			"Отмена": function() {
				$(this).dialog("close");
			}
		}
	});


	$( "#login_forgot_password" ).click(function(){
		$('#login_forgot_pass_dialog').dialog( "open" );
	});

	$('#signin_add_country').on('keyup', function(){
		var country = $(this).val();

		$.ajax({
			url: '/user/get_similar_country',
			dataType: 'json',
			data: {country: country, csrf_hobby_name:csrf_hobby_value},
			type: 'post',
			success: function(data){
				$(".autocomplete_add_country").autocomplete({
					source: data,
					delay: 300,
				});
			}
		});
	});

	$('#signin_hobby_tag').on('keyup', function(){
		var hobby = $(this).val();

		$.ajax({
			url: '/user/get_similar_hobby',
			dataType: 'json',
			data: {hobby: hobby, csrf_hobby_name:csrf_hobby_value},
			type: 'post',
			success: function(data){
				$(".autocomplete_add_hobby").autocomplete({
					source: data,
					delay: 300,
				});
			}
		});
	});

	$('#login_form').submit(function(){
		//Валидация заполнения имя, имейла, пароля
		var error = false;
		$('.login_main_inputs').each(function(){
			if($(this).val().length == 0) {
				error = true;
			}
		});

		if(error) {
			$('#login_error_field').text('Заполните обязательные поля');
			$('#login_error_field').show();
			$('.login_main_inputs').addClass('is-invalid');
			return false;
		}
	});

	$('#signin_form').submit(function(){
		$('#signin_error_field').text('');
		$('#signin_error_field').hide();


		//Валидация заполнения имя, имейла, пароля
		var error = false;
		$('.sign_in_main_inputs').each(function(){
			if($(this).val().length == 0) {
				$(this).addClass('is-invalid');
				error = true;
			}
		});

		if(error) {
			$('#signin_error_field').text('Заполните обязательные поля');
			$('#signin_error_field').show();
			$(document).scrollTop(0);
			return false;
		}

		//Валидация паттерна имейла
		var re = /\S+@\S+\.\S+/;
		if(!re.test($('input[name=email]').val())){
			$('#signin_error_field').text('Неверно введен Email');
			$('#signin_error_field').show();
			$(document).scrollTop(0);
			return false;
		}


		//Проверка не существует ли уже такой имейл
		var email = $('input[name=email]').val();
		$.ajax({
			url: '/user/is_email_exists',
			dataType: 'json',
			data: {email: email, csrf_hobby_name:csrf_hobby_value},
			type: 'post',
			success: function(data){
				if(!Boolean(data)) {
					$('#signin_error_field').text('Email уже существует');
					$('#signin_error_field').show();
					$(document).scrollTop(0);
					return false;
				}
			}
		});


		//Валидация хобби тегов. Посчитываем кол. в скрытых инпутах, если они все пустые то ошибка
		var hobby_tags = [];
		$('.signin_hobby_tag').each(function(){
			if($(this).val() == '') var res = null;
			else res = $(this).val()
			hobby_tags.push(res);
		});

		var none_tags = 0;
		for(var i = 0; i < hobby_tags.length; i++){
			if(hobby_tags[i] === null) none_tags++;
		}

		if(none_tags == 5) {
			$('#signin_error_field').text('Напишите ваши хобби');
			$('#signin_error_field').show();
			$('#signin_hobby_tag').addClass('is-invalid');
			$(document).scrollTop(0);
			return false;
		}

		//Валидация загрузки картинки
		if($("#sign_in_picture_upload")[0].files.length === 0){
			$('#signin_error_field').text('Загрузите ваше фото');
			$('#signin_error_field').show();
			$(document).scrollTop(0);
			return false;
		}


		//Валидация кропа
		/*
		if (parseInt($('#w').val())) return true;
		else {
				alert('Please select a crop region then press submit.');
				return false;
			}*/

		return true;
	});

	//Добавление тегов при регистрации, редактировании
	$('#signin_add_hobby_tag').click(function(){
		if(($('#signin_block_hobby_tags').find('.badge-custom').length == 5) || $('#signin_hobby_tag').val().length < 3) {
			$('#signin_max_hobby_alert').css('color', '#FF6347');
			return false;
		}

		var tag = $('#signin_hobby_tag').val();
		$('#signin_block_hobby_tags').append('<span class="badge-custom">' + tag + '<i class="fa fa-times signin_remove_hobby_tag" data-tag="' + tag + '" aria-hidden="true"></i></span>');

		$('.signin_hobby_tag').each(function(){
			if(!$(this).val()) {
				$(this).val(tag);
				return false;
			}
		});

		$('#signin_hobby_tag').val('');
	});

	//Удаление тегов при регистрации, редактировании
	$(document).on('click', '.signin_remove_hobby_tag', function(){
		$(this).parent().remove();
		var remove_tag = $(this).attr('data-tag')

		$('.signin_hobby_tag').each(function(){
			if($(this).val() == remove_tag) {
				$(this).val('');
				return false;
			}
		});
	});

	//Отправка файла при регистрации
	$('#sign_in_picture_upload').on('change', function() {
    var file_data = $('#sign_in_picture_upload').prop('files')[0];
    var form_data = new FormData();
    form_data.append('file', file_data);
	form_data.append('csrf_hobby_name', csrf_hobby_value);

    $.ajax({
		url: '/user/download_picture',
		dataType: 'json',
		cache: false,
		contentType: false,
		processData: false,
		data: form_data,
		type: 'post',
		beforeSend: function(){
			$('#signin_upload_pic_bar').show();
		},
		success: function(data){

			if ($('#signin_block_for_upload_pic img').data('Jcrop')) {
				$('#cropbox').data('Jcrop').destroy();
				$('#cropbox').remove();
			}

			var host = document.location.origin;
			$('#signin_block_for_upload_pic').append('<img class="img-fluid" id="cropbox" src="' + host + '/application/images/users/' + data.file_name + '" />');
			$('#picture_name').val(data.file_name);


			$('#cropbox').Jcrop({
				 aspectRatio: 1,
				 onChange: updateCoords,
				 //onSelect: checkCoords,
				 setSelect: [0, 160, 160, 0],
				 minSize: [230, 230],
				 trueSize: [data.new_width,data.new_height]
				 //maxSize: [230, 230],

			});

			$('#signin_upload_pic_bar').hide();
		}
     });
	});







    $('#show_more_radio').click(function(){
        var counter = $(this).attr('data-counter-records');
        var full_href = window.location.href;
        var host = window.location.hostname;

        var href = full_href.split('/');

        if(href[4] == 'ukraine') var param = 'ukraine';
        else if(href[4] == 'foreign') var param = 'foreign';
        else var param = 'all';

        $.ajax({
            url: '/radio/get_more_radios',
            type: 'POST',
            data: {filter:param, page:counter},
            dataType: 'json',
            success: function(data){
                counter++;
                $('#show_more_radio').attr('data-counter-records', counter);

                var html = "";
                for(var i in data){
                    html += '<div class="col-lg-3 col-md-3 col-sm-6 col-xs-6 station_block station_block_margin_top"  data-stream="' + data[i]['stream_link'] +  '">';
                    html += '<div class="station_subblock" >';
                    html += '<ul>';
                    html += '<li class="links"> <a href="' + full_href + '/index/' +  data[i]['alias'] + '" >';
                    html += '<img alt="Радио ' + data[i]['name'] +  '" class="main_page_radios_img" title="' + data[i]['name'] + ', '  + data[i]['genre_name'] + '" rel="tooltip" class="img-responsive" src="http://' + host + '/application/images/stations/' + data[i]['picture'] + '"  /></a>';
                    html += '<button style="background-image:none;" class="btn btn-lg btn-success playButton playButtonMainPage" id="playButton" data-alias="' +  data[i]['alias'] +  '" data-title="' + data[i]['name'] + '" data-stream="' + data[i]['stream_link'] + '">Слушать</button>';
                    html += '</li>';
                    html += '</ul>';
                    html += '</div>';
                    html += '</div>';
                }

                $('.all_pages_text').prev().children().last().after(html);
            },
            fail: function(){

            }

        });
    });



});








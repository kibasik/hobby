User-agent: *

Disallow: /admin
Disallow: /main/search
Disallow: /main/error404
Disallow: /radio/current_song
Disallow: /radio/carousel
Disallow: /news
Disallow: /news
Disallow: /news/week
Disallow: /news/month
Disallow: /news/save_comment

Disallow: /libraries
Disallow: /js
Disallow: /css
Disallow: /images
Disallow: /system
Disallow: /user_guide
Disallow: /logs
Disallow: /third_party
Disallow: /language


Sitemap:
Host: whalebeat.com
